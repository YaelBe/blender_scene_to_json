import bpy
import json
import os
import mathutils
import math
from math import pi

def transformPos(v):
    X = [[0, -1, 0], [0, 0, 1], [-1, 0, 0]]
    result = [0, 0, 0]
    for i in range(len(X)):
        for k in range(len(v)):
           result[i] += X[i][k] * v[k]
    return result
      
def write_some_data(context, filepath, use_some_setting):
    print("running write_some_data...")
    f = open(filepath, 'w', encoding='utf-8')
    directory = os.path.dirname(filepath)
    f2 = open(directory+"/test.txt", 'w', encoding='utf-8')
    dict = {}
    dict["children"] = []
    dict["name"] = "root"
    objArray = []
    assetIndex = 0
    objs = []
    for ob in bpy.context.scene.objects:
        objs.append(ob)
    dataDict = {}
    excludeList = []#["Bg", "Fog", "Particle"]
    shouldWriteAssets = True
    cameraLocation = (0, 0, 0)
    cameraRotation = (0, 0, 0, 0)
    cameraSet = False
    for ob in objs:
        if ob.type=='CAMERA':
            cameraLocation = ob.location
            cameraRotation = ob.rotation_euler
            p1 = [cameraLocation.x, cameraLocation.y, cameraLocation.z]
            e1 = mathutils.Euler(cameraRotation)
            e0a = mathutils.Euler([-pi/2, 0, 0])
            e0b = mathutils.Euler([0, pi/2, 0])
            q = e0a.to_quaternion()
            q.rotate(e0b.to_quaternion())
            q.rotate(e1.to_quaternion())
            axis1 = transformPos([q.to_axis_angle()[0][0], q.to_axis_angle()[0]
            [1], q.to_axis_angle()[0][2]])
            r1 = [axis1[0], axis1[1], axis1[2], q.to_axis_angle()[1]]
            obResult = {"name":ob.name, "type":'Camera', "position":transformPos(p1), "rotation":r1}
#            if ob.parent:
#                for obD in objArray:
#                    if obD["obj"]==ob.parent:
#                        obD["children"].append(obResult)
#                        break
#                f.write("object name:  "+ ob.data.name+": "+ob.parent.name+"\r\n")
#            else:
            dict["children"].append(obResult)
        if ob.type=='MESH' or ob.type=='EMPTY':
            bpy.ops.object.select_all(action='DESELECT')
            s1 = [ob.scale[1], ob.scale[2], ob.scale[0]]

            e1 = mathutils.Euler(ob.rotation_euler)
            axis1 = transformPos([e1.to_quaternion().to_axis_angle()[0][0], e1.to_quaternion().to_axis_angle()[0]
            [1], e1.to_quaternion().to_axis_angle()[0][2]])
            r1 = [axis1[0], axis1[1], axis1[2], e1.to_quaternion().to_axis_angle()[1]]
            if ob.type=='MESH':
                if dataDict.get(ob.data.name)==None:
                    newfile_name = os.path.join( directory , "asset"+str(assetIndex)+".dae")
                    assetName = "asset"+str(assetIndex)
                    assetIndex+=1
                    dataDict[ob.data.name] = assetName
                    shouldWriteThis = shouldWriteAssets
                    for excludeStr in excludeList:
                        if ob.name.startswith(excludeStr):
                            assetName = ""
                            shouldWriteThis = False
                            break
                    if shouldWriteThis:
                        C = bpy.context
                        new_obj = ob.copy()
                        new_obj.parent = None
                        new_obj.data = ob.data.copy()
                        new_obj.animation_data_clear()
                        new_obj.location = (0, 0, 0)
                        f2.write(str(new_obj.scale))
                        new_obj.scale = (1, 1, 1)
                        new_obj.rotation_mode = 'XYZ'
                        new_obj.rotation_euler = (0, 0, 0)
                        C.collection.objects.link(new_obj)
                        new_obj.select_set(True)
                        bpy.context.view_layer.objects.active = new_obj
                        bpy.ops.object.editmode_toggle() #enter edit mode
                        bpy.ops.mesh.select_all(action='SELECT') #select all objects elements
                        #bpy.ops.mesh.normals_make_consistent(inside=False) #recalc normals
                        bpy.ops.object.editmode_toggle() #exit edit mode
                        bpy.ops.wm.collada_export(filepath=newfile_name, selected=True, export_global_up_selection='Y', export_global_forward_selection='-X')
                        #new_obj.select_set(False)
                        bpy.ops.object.delete()
                else:
                    assetName = dataDict[ob.data.name]
                for excludeStr in excludeList:
                    if ob.name.startswith(excludeStr):
                        assetName = ""
                        break
                #f2.write(""+assetName+": "+ob.data.name+", rotationMode: "+ob.rotation_mode+", ("+str(r1[0])+", "+str(r1[1])+", "+str(r1[2])+")\r\n")
            dictChildren = []
            objArray.append({"obj": ob, "children":dictChildren })
            p1 = [ob.location.x, ob.location.y, ob.location.z]
            if ob.parent:
                p1 = [p1[0]-ob.parent.location.x, p1[1]-ob.parent.location.y, p1[2]-ob.parent.location.z]
                e1 = mathutils.Euler(ob.rotation_euler)
                q = e1.to_quaternion() @ mathutils.Euler(ob.parent.rotation_euler).to_quaternion().inverted()
                axis1 = transformPos([q.to_axis_angle()[0][0], q.to_axis_angle()[0]
                [1], q.to_axis_angle()[0][2]])
                r1 = [axis1[0], axis1[1], axis1[2], q.to_axis_angle()[1]]
            obResult = {"name":ob.name, "children":dictChildren, "position":transformPos(p1), "rotation":r1, "scale":(s1)}
            if ob.type=='MESH':
                obResult["assetName"] = assetName
            if ob.parent:
                for obD in objArray:
                    if obD["obj"]==ob.parent:
                        obD["children"].append(obResult)
                        break
#                f.write("object name:  "+ ob.data.name+": "+ob.parent.name+"\r\n")
            else:
                dict["children"].append(obResult)
#                f.write("object name:  "+ ob.data.name+"\r\n")
    json_object = json.dumps(dict, indent = 4)
    f.write(json_object)
    f.close()
    f2.close()

    return {'FINISHED'}


# ExportHelper is a helper class, defines filename and
# invoke() function which calls the file selector.
from bpy_extras.io_utils import ExportHelper
from bpy.props import StringProperty, BoolProperty, EnumProperty
from bpy.types import Operator


class ExportSomeData(Operator, ExportHelper):
    """This appears in the tooltip of the operator and in the generated docs"""
    bl_idname = "export_test.some_data"  # important since its how bpy.ops.import_test.some_data is constructed
    bl_label = "Export Some Data"

    # ExportHelper mixin class uses this
    filename_ext = ".txt"

    filter_glob: StringProperty(
        default="*.txt",
        options={'HIDDEN'},
        maxlen=255,  # Max internal buffer length, longer would be clamped.
    )

    # List of operator properties, the attributes will be assigned
    # to the class instance from the operator settings before calling.
    use_setting: BoolProperty(
        name="Example Boolean",
        description="Example Tooltip",
        default=True,
    )

    type: EnumProperty(
        name="Example Enum",
        description="Choose between two items",
        items=(
            ('OPT_A', "First Option", "Description one"),
            ('OPT_B', "Second Option", "Description two"),
        ),
        default='OPT_A',
    )

    def execute(self, context):
        return write_some_data(context, self.filepath, self.use_setting)


# Only needed if you want to add into a dynamic menu
def menu_func_export(self, context):
    self.layout.operator(ExportSomeData.bl_idname, text="Text Export Operator")

# Register and add to the "file selector" menu (required to use F3 search "Text Export Operator" for quick access)
def register():
    bpy.utils.register_class(ExportSomeData)
    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)


def unregister():
    bpy.utils.unregister_class(ExportSomeData)
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)


if __name__ == "__main__":
    register()

    # test call
    bpy.ops.export_test.some_data('INVOKE_DEFAULT')
